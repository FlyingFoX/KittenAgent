// ==UserScript==
// @author      Matthias Fritsche
// @name        Kitten Agent
// @namespace   FlyingFoX
// @include     http://bloodrizer.ru/games/kittens/*
// @description An Agent to help automate the boring parts of the kittens game
// @version     1
// @grant       none
// ==/UserScript==
var loopMs = 2 * 1000;
function notifyCatpower() {
    new Notification("catpower is almost full!");
};
function tryNotify(notificationFn) {
    if (Notification.permission === "denied"){
        return;
    } else if (Notification.permission !== "granted"){
        Notification.requestPermission(function(permission) {
            if (permission === "granted") {
                notificationFn();
            }
        });
    } else { notificationFn();}
};
function isResourceNearMax(resource){
    // there are 5 ticks per second.
    // add 1 second to the loop time to prevent reaching the maximum.
    return resource.value > (resource.maxValue - 5 * (loopMs/1000 + 1) * resource.perTickCached);
}
/// reads configuration.#{resourceName}.automate from localStorage and converts it to bool
function shouldAutomate(resourceName){
    return localStorage.getItem(`configuration.${resourceName}.automate`) === 'true';
}
function standardWatcher(resName, craftName){
    if(!shouldAutomate(resName)) {
        return;
    }
    var resource = gamePage.resPool.get(resName);
    if (isResourceNearMax(resource)){
        craftAll(craftName);
    }
}
function craftAll(craftName){
    if (gamePage.resPool.get(craftName).unlocked) {
        gamePage.craftAll(craftName);
    }
}
// only try to enable notifications if permissions have not been granted yet.
if (Notification.permission !== "granted"){
    tryNotify(function() {new Notification("Notifications successfully enabled!");});
}
// add indication that the Kitten Agent has been started
$("#motd").after(`<span id="agent"><a href="#" onclick="$('#agentDiv').show()">Kitten Agent</a> <span style="color: green;">version: 0.1.8</span></span>`);
function addSettingsMenu(){
    var ids = [
        ["configuration.wood.automate", "Wood"],
        ["configuration.minerals.automate", "Minerals"],
        ["configuration.iron.automate", "Iron"],
        ["configuration.coal.automate", "Coal"],
        ["configuration.stars.automate", "Stars"],
        ["configuration.culture.automate", "Culture" ],
        ["configuration.hunters.automate", "Hunters"],
        ["configuration.science.automate", "Science"],
        ["configuration.oil.automate", "Oil"],
        ["configuration.uranium.automate", "Uranium"],
        ["configuration.faith.automate", "Faith"],
        ["configuration.blueprint.automate", "Blueprint"],
        ["configuration.unobtainium.automate", "Unobtainium"],
        ["configuration.titanium.automate", "Titanium"]
    ];
    var settings = "";
    ids.forEach(function(id){
        settings = settings +
            `<div style="float: left"><input class="configuration automate" type="checkbox" id="${id[0]}">
<label for="${id[0]}">${id[1]}</label></div>`;
    });
    settings = `<fieldset><legend>Kitten Agent Settings</legend>${settings}</fieldset>`;
    var agentDiv =
`<div id="agentDiv" class="agentOptions">
    <a href="#" onclick="$('#agentDiv').hide()" class="closeButton" style="position: absolute; top: 10px; right: 15px;">close</a>
    ${settings}
</div>`;
    $("#rightColumn").prepend(agentDiv);
    $("input.configuration.automate").each(function(idx, input){
        input.checked = localStorage.getItem(input.id) === 'true';
    });
    $("input.configuration.automate").click(function(){
        localStorage.setItem($(this).prop("id"), $(this).prop("checked"));
    });
    // correct checkboxes to display checked and unchecked in the same size
    var customCSS = `
.scheme_sleek input[type="checkbox"] + label::before {
  font-size: 14px;
  margin-left: 6px;
}
.scheme_sleek input[type="checkbox"]:checked + label::before {
  font-size: 14px;
  margin-right: 1px;
}`;
    var style = document.createElement("style");
    style.type = "text/css";
    style.innerHTML = customCSS;
    document.head.appendChild(style);
}
addSettingsMenu();
function watchCatpower(){
    if (!shouldAutomate("hunters")){
        return;
    }
    var catpower = gamePage.resPool.get("manpower");
    if (isResourceNearMax(catpower)){
        $("#fastHuntContainer a").click();
        //tryNotify(notifyCatpower);
    }
};
function watchStar() {
    if (!shouldAutomate("stars")){
        return;
    }
    var observeButton = $("#observeButton").find("#observeBtn");
    if (observeButton.length >  0) {
        $("#observeBtn").click();
        //tryNotify(function(){new Notification("A rare astronomical event occured in the sky!");});
    }
};
function watchCoalAndIron() {
    var automateCoal = shouldAutomate("coal");
    var automateIron = shouldAutomate("iron");
    var coal = gamePage.resPool.get("coal");
    var iron = gamePage.resPool.get("iron");
    if (automateCoal && automateIron) {
        if (isResourceNearMax(iron)){
            craftAll("steel");
            if (automateIron){
                craftAll("plate");
            }
        }
        if (isResourceNearMax(coal)){
            craftAll("steel");
        }
    }
    else if (automateCoal) {
        if (isResourceNearMax(coal)){
            craftAll("steel");
        }
    }
    else if (automateIron) {
        if (isResourceNearMax(iron)){
            craftAll("plate");
        }
    }
}
function watchFaith(){
    if (!shouldAutomate("faith")){
        return;
    }
    var faith = gamePage.resPool.get("faith");
    if (isResourceNearMax(faith)) {
        $("#fastPraiseContainer a").click();
    }
}
function watchBlueprint(){
    if (!shouldAutomate("blueprint")){
        return;
    }
    var science = gamePage.resPool.get("science");
    if (isResourceNearMax(science)){
        craftAll("blueprint");
    }
}
var agentLoop = setInterval(function() {
    watchCatpower();
    watchStar();
    watchCoalAndIron();
    watchFaith();
    standardWatcher("culture", "manuscript");
    standardWatcher("wood", "beam");
    standardWatcher("minerals", "slab");
    // it is really called 'compedium' and not 'compendium'!
    standardWatcher("science", "compedium");
    standardWatcher("oil", "kerosene");
    standardWatcher("uranium", "thorium");
    watchBlueprint();
    standardWatcher("titanium", "alloy");
    standardWatcher("unobtainium", "eludium");
}, loopMs);
